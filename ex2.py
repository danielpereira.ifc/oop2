# Classe simples
class Pessoa:
    def __init__(self, nome, idade):
        self.nome = nome
        self.idade = idade
    
    def aniversario(self):
        self.idade += 1
        print(f"Parabéns, {self.nome}! Agora você tem {self.idade} anos.")

# Classe que usa herança
class Estudante(Pessoa):
    def __init__(self, nome, idade, curso):
        super().__init__(nome, idade)
        self.curso = curso
    
    def estudar(self):
        print(f"{self.nome} está estudando {self.curso}.")

# Classe que usa composição
class Universidade:
    def __init__(self, nome, localizacao):
        self.nome = nome
        self.localizacao = localizacao
    
    def adicionar_estudante(self, estudante):
        print(f"{estudante.nome} está estudando na {self.nome}.")

# Função para inspecionar uma classe
def inspecionar_classe(classe):
    print(f"Atributos da classe {classe.__name__}:")
    print(dir(classe))

# Função para executar um método em um objeto
def executar_metodo(objeto, nome_metodo, *args):
    if hasattr(objeto, nome_metodo):
        metodo = getattr(objeto, nome_metodo)
        if callable(metodo):
            metodo(*args)
        else:
            print(f"O atributo {nome_metodo} não é um método.")
    else:
        print(f"O objeto {objeto.__class__.__name__} não possui o método {nome_metodo}.")

# Criando objetos
pessoa1 = Pessoa("João", 20)
estudante1 = Estudante("Maria", 19, "Engenharia")
universidade1 = Universidade("Universidade Federal de Minas Gerais", "Belo Horizonte")

# Chamando métodos
pessoa1.aniversario() # Parabéns, João! Agora você tem 21 anos.
estudante1.estudar() # Maria está estudando Engenharia.
universidade1.adicionar_estudante(estudante1) # Maria está estudando na Universidade Federal de Minas Gerais.

# Inspecionando as classes
inspecionar_classe(Pessoa)
inspecionar_classe(Estudante)
inspecionar_classe(Universidade)

# Executando métodos dinamicamente
executar_metodo(pessoa1, "aniversario") # Parabéns, João! Agora você tem 22 anos.
executar_metodo(estudante1, "estudar") # Maria está estudando Engenharia.
executar_metodo(universidade1, "adicionar_estudante", estudante1) # Maria está estudando na Universidade Federal de Minas Gerais.
executar_metodo(pessoa1, "estudar") # O objeto Pessoa não possui o método estudar.
