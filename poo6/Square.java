public class Square{
    public float edge;

    /**
     * Class that represents a Square
     * 
     * @param edge represents the edge of the Square
     */
    
    public Square(float edge) {
        this.edge = edge;
    }

    public void print_square() {
        System.out.println("Edge:" + this.edge);
    }
}