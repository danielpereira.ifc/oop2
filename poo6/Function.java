public class Function  {
    
    private Square pants;

    /**
     * @param pants Discribes the square
     */
    public Function(Square pants) {
        this.pants = pants;
    }

    /**
     * 
     * @return Prints the square perimeter
     */
    public void calculate_perimeter() {
        System.out.println (4*pants.edge);
    }
}