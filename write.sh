#!/bin/sh

echo "class name: "
read -r class_name

touch "$class_name".h

# insert macro into the file in order to not repeat include files later on
echo "#ifndef h_$class_name" >  "$class_name".h
echo "#define h_$class_name" >> "$class_name".h
echo "#endif"                >> "$class_name".h

# insert the class signature into the 3rd line of the file
sed "3s/^/\nclass\ $class_name \n{\n};\n\n/" "$class_name".h > /tmp/"$class_name"
mv /tmp/"$class_name" "$class_name".h

# variable to store the choices the user picked
pick=-1

# function to give members to the class (such as constructors, methods and atributes 
# as well as its respective encapsulation options)
meaningful_name() {
    while true; do
        # convey five options for the user to choose from
        echo "options: "
        echo '(0) exit'
        echo '(1) insert member'
        echo '(2) insert member function'
        echo '(3) insert constructor'
        echo '(4) print'
        echo "choose an option: "
        read -r pick
        printf "\n\n"

# constructor option
        if [ "$pick" = 3 ]; then
            printf '\n\n*******not implemented********\n\n\n'
        fi


# exit option
        if [ $pick = 0 ]; then
            break
        fi

# read file option
        if [ $pick = 4 ]; then
            printf "\n\n-----------------------\n"
            cat -n "$class_name".h
            printf "\-----------------------\n\n\n\n\n"
        fi

# insert member function option
        if [ "$pick" = 2 ]; then
            # print three options of encapsulation for the user to pick from
            echo    "encapsulation option: "
            echo    "(a) public"
            echo    "(b) protected"
            echo    "(c) private"
            echo "pick an encapsulation option: "
            read -r encapsulation
            printf  "\n\n"


            # private option
            if [ "$encapsulation" = "c" ]; then
                if [ "$(grep 'private:' "$class_name".h)" != "" ]; then
                # there already is a private statement
                    printf "\n\n"
                    echo "enter the member function type and identificator: "
                    read -r func_name
                    # get the number of the line where it found the expression 'private'
                    line_number="$(grep -n 'private m' "$class_name".h | sed "s/[a-zA-Z:{/]//g")"
                    # insert content into <line_number> line
                    sed "$line_number s/^.*/\t\/\/private members:\n\t$func_name();/" "$class_name".h > /tmp/"$class_name"
                    mv /tmp/"$class_name" "$class_name".h
                fi


                if [ "$(grep 'private:' "$class_name".h)" = "" ]; then
                # there is not a private statment
                    # get the number of the line where it found the '{' character
                    line_number="$(grep -n '{' "$class_name".h | sed "s/[a-zA-Z:{/]//g")"
                    # insert content into the line number obtained in the command of the previous line
                    sed "$line_number s/^.*/{\n\tprivate:\n\t\/\/private members:/" "$class_name".h > /tmp/"$class_name"
                    mv /tmp/"$class_name" "$class_name".h
                    printf "\n\n"
                    echo "enter the member function type and identificator: "
                    read -r func_name
                    # get the number of the line where it found the expression 'private'
                    line_number="$(grep -n 'private members:' "$class_name".h | sed "s/[a-zA-Z:{/]//g")"
                    sed "$line_number s/^.*/\t\/\/private members:\n\t$func_name();/" "$class_name".h > /tmp/"$class_name"
                    mv /tmp/"$class_name" "$class_name".h
                fi
            fi




            # public option
            if [ "$encapsulation" = "a" ]; then
                if [ "$(grep 'public:' "$class_name".h)" != "" ]; then
                    printf "\n\n"
                    echo "enter the member function type and identificator: "
                    read -r func_name
                    # get the number of the line where it found the expression 'public members:'
                    line_number="$(grep -n 'public members:' "$class_name".h | sed "s/[a-zA-Z:{/]//g")"
                    # insert content into <line_number> line, and
                    sed "$line_number s/^.*/\t\/\/public members:\n\t$func_name();/" "$class_name".h > /tmp/"$class_name"
                    mv /tmp/"$class_name" "$class_name".h
                fi


                if [ "$(grep 'public:' "$class_name".h)" = "" ]; then
                # there is not a public statment
                    # get the number of the line where it found the '{' character
                    line_number="$(grep -n '{' "$class_name".h | sed "s/[a-zA-Z:{/]//g")"
                    # insert content into <line_number> line
                    sed "$line_number s/^.*/{\n\tpublic:\n\t\/\/public members:/" "$class_name".h > /tmp/"$class_name"
                    mv /tmp/"$class_name" "$class_name".h
                    printf "\n\n"
                    echo "enter the member function type and identificator: "
                    read -r func_name
                    # get the number of the line where it found the expression 'public'
                    line_number="$(grep -n 'public members:' $class_name.h | sed "s/[a-zA-Z:{/]//g")"
                    sed "$line_number s/^.*/\t\/\/public members:\n\t$func_name();/" "$class_name".h > /tmp/"$class_name"
                    mv /tmp/"$class_name" "$class_name".h
                fi
            fi

            # protected option
            if [ "$encapsulation" = "b" ]; then
                if [ "$(grep 'protected:' "$class_name".h)" != "" ]; then
                    printf "\n\n"
                    echo "enter the member function type and identificator: "
                    read -r func_name
                    # get the number of the line where it found the expression 'protected'
                    line_number="$(grep -n 'protected m' "$class_name".h | sed "s/[a-zA-Z:{/]//g")"
                    # insert content into <line_number> line, and
                    sed "$line_number s/^.*/\t\/\/protected members:\n\t$func_name();/" "$class_name".h > /tmp/"$class_name"
                    mv /tmp/"$class_name" "$class_name".h
                fi


                if [ "$(grep 'protected:' "$class_name".h)" = "" ]; then
                # there is not a protected statment
                    # get the number of the line where it found the '{' character
                    line_number="$(grep -n '{' "$class_name".h | sed "s/[a-zA-Z:{/]//g")"
                    # insert content into <line_number> line
                    sed "$line_number s/^.*/{\n\tprotected:\n\t\/\/protected members:/" "$class_name".h > /tmp/"$class_name"
                    mv /tmp/"$class_name" "$class_name".h
                    printf "\n\n"
                    echo "enter the member function type and identificator: "
                    read -r func_name
                    # get the number of the line where it found the expression 'protected'
                    line_number="$(grep -n 'protected\ ' "$class_name".h | sed "s/[a-zA-Z:{/]//g")"
                    sed "$line_number s/^.*/\t\/\/protected members:\n\t$func_name();/" "$class_name".h > /tmp/"$class_name"
                    mv /tmp/"$class_name" "$class_name".h
                fi
            fi
        fi








# insert member option
        if [ "$pick" = 1 ]; then
            echo    "encapsulation option: "
            echo    "(a) public"
            echo    "(b) protected"
            echo    "(c) private"
            echo "pick an encapsulation option: "
            read -r encapsulation
            printf  "\n\n"

            # public option
            if [ "$encapsulation" = "a" ]; then
                if [ "$(grep 'public' "$class_name".h)" != "" ]; then
                    printf "\n\n"
                    echo "enter the atribute type and identificator: "
                    read -r atrib_name
                    # get the number of the line where it found the expression 'public'
                    line_number="$(grep -n 'public members:' "$class_name".h | sed "s/[a-zA-Z:{/]//g")"
                    # insert content into <line_number> line, and
                    sed "$line_number s/^.*/\t\/\/public members:\n\t$atrib_name;/" "$class_name".h > /tmp/"$class_name"
                    mv /tmp/"$class_name" "$class_name".h
                fi


                if [ "$(grep 'public' "$class_name".h)" = "" ]; then
                # there is not a public statment
                    # get the number of the line where it found the '{' character
                    line_number="$(grep -n '{' "$class_name".h | sed "s/[a-zA-Z:{/]//g")"
                    # insert content into <line_number> line
                    sed "$line_number s/^.*/{\n\tpublic:\n\t\/\/public members:/" "$class_name".h > /tmp/"$class_name"
                    mv /tmp/"$class_name" "$class_name".h
                    printf "\n\n"
                    echo "enter the atribute type and identificator: "
                    read -r atrib_name
                    # get the number of the line where it found the expression 'public'
                    line_number="$(grep -n 'public members:' "$class_name".h | sed "s/[a-zA-Z:{/]//g")"
                    sed "$line_number s/^.*/\t\/\/public members:\n\t$atrib_name;/" "$class_name".h > /tmp/"$class_name"
                    mv /tmp/"$class_name" "$class_name".h
                fi
            fi


            # protected option
            if [ "$encapsulation" = "b" ]; then
                if [ "$(grep 'protected' "$class_name".h)" != "" ]; then
                    printf "\n\n"
                    echo "enter the atribute type and identificator: "
                    read -r atrib_name
                    # get the number of the line where it found the expression 'protected'
                    line_number="$(grep -n 'protected members:' "$class_name".h | sed "s/[a-zA-Z:{/]//g")"
                    # insert content into <line_number> line, and
                    sed "$line_number s/^.*/\t\/\/protected members:\n\t$atrib_name;/" "$class_name".h > /tmp/"$class_name"
                    mv /tmp/"$class_name" "$class_name".h
                fi


                if [ "$(grep 'protected' "$class_name".h)" = "" ]; then
                # there is not a protected statment
                    # get the number of the line where it found the '{' character
                    line_number="$(grep -n '{' "$class_name".h | sed "s/[a-zA-Z:{/]//g")"
                    # insert content into <line_number> line
                    sed "$line_number s/^.*/{\n\tprotected:\n\t\/\/protected members:/" "$class_name".h > /tmp/"$class_name"
                    mv /tmp/"$class_name" "$class_name".h
                    printf "\n\n"
                    echo "enter the atribute type and identificator: "
                    read -r atrib_name
                    # get the number of the line where it found the expression 'protected'
                    line_number="$(grep -n 'protected members:' "$class_name".h | sed "s/[a-zA-Z:{/]//g")"
                    sed "$line_number s/^.*/\t\/\/protected members:\n\t$atrib_name;/" "$class_name".h > /tmp/"$class_name"
                    mv /tmp/"$class_name" "$class_name".h
                fi
 
            fi
            # private option
            if [ "$encapsulation" = "c" ]; then
                if [ "$(grep 'private' "$class_name".h)" != "" ]; then
                # there is a private statement
                    printf "\n\n"
                    echo "enter the atribute type and identificator: "
                    read -r atrib_name
                    # get the number of the line where it found the expression 'private'
                    line_number="$(grep -n 'private members:' "$class_name".h | sed "s/[a-zA-Z:{/]//g")"
                    # insert content into <line_number> line, and
                    sed "$line_number s/^.*/\t\/\/private members:\n\t$atrib_name;/" "$class_name".h > /tmp/"$class_name"
                    mv /tmp/"$class_name" "$class_name".h
                fi


                if [ "$(grep 'private' "$class_name".h)" = "" ]; then
                # there is not a private statment
                    # get the number of the line where it found the '{' character
                    line_number="$(grep -n '{' "$class_name".h | sed "s/[a-zA-Z:{/]//g")"
                    # insert content into <line_number> line
                    sed "$line_number s/^.*/{\n\tprivate:\n\t\/\/private members:/" "$class_name".h > /tmp/"$class_name"
                    mv /tmp/"$class_name" "$class_name".h
                    printf "\n\n"
                    echo "enter the atribute type and identificator: "
                    read -r atrib_name
                    # get the number of the line where it found the expression 'private'
                    line_number="$(grep -n 'private members:' "$class_name".h | sed "s/[a-zA-Z:{/]//g")"
                    sed "$line_number s/^.*/\t\/\/private members:\n\t$atrib_name;/" "$class_name".h > /tmp/"$class_name"
                    mv /tmp/"$class_name" "$class_name".h
                fi
 
            fi
        fi

        # exit if user inputs 0
        if [ "$pick" = 0 ]; then
            break
        fi
    done # end of loop
}
meaningful_name

